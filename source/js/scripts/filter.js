
$(document).ready(function () {
  (function () {
    var item = $('.js-filter-block');

    function init_filter() {

      // обрабатываем клик по заголовку
      item.on('click', '.title', function () {
        var $this = $(this),
          parent = $this.closest('.js-filter-block');

        if (parent.hasClass('opening')) {
          parent
            .removeClass('opening')
            .addClass('closing');
        } else {
          parent
            .removeClass('closing')
            .addClass('opening');
        }
      });
    }

    if (item.length > 0) {
      return init_filter();
    }
  }());

});